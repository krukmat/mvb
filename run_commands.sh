#!/bin/sh
service nginx restart
./run_embark.sh
cd /code
python manage.py makemigrations
python manage.py migrate
python manage.py runserver 0.0.0.0:8080
exec "$@"