# MVB #

A POC that allow users to register and manage micro bank platform. It's built in EmbarkJS as layer between EVM (Ethereum Virtual Machine) and standard js framework: angularjs.
Then there's a django REST service to integrate the standard web authentication. The frontend is build in AngularJS.

### Docker Usage ###

```
Clean it up: docker rm $(docker ps -a -q)
Build: docker-compose build
Run: docker-compose up
Bash: docker exec -i -t ca5fb3bb410a /bin/bash
```

Installation
============

Local Terminal:
```
    * docker-compose up
    * docker ps -> check the id
    * docker exec -i -t b1bce6a87fe0 /bin/bash
```
In docker instance:
```
root@web:/code# ./update_angularjs_layout.sh
root@web:/code# python manage.py shell
```
In python:
```python
from backend.models import *
user = MyPointsUser.objects.create_superuser('amail@mail.com','12345')
```
In browser: 
```
http://docker_ip/webapp/#!/ -> use user created in the step before.
```

### Who do I talk to? ###
* Matias Kruk <kruk.matias at gmail.com>