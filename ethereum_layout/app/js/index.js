/*globals $, SimpleStorage, document*/

var addToLog = function(id, txt) {
  $(id + " .logs").append("<br>" + txt);
};

// ===========================
// Blockchain example
// ===========================
$(document).ready(function() {
  SimpleStorage.set(5);
  SimpleStorage.get().then(function(value) {
      console.log("The value is: ", value.toNumber());
  });
});
